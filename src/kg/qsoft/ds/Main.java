package kg.qsoft.ds;

import com.bulenkov.darcula.DarculaLaf;
import kg.qsoft.ds.preference.Registry;
import kg.qsoft.ds.ui.LoginForm;
import kg.qsoft.ds.ui.MainForm;

import javax.swing.*;

/**
 * Created by akoikelov
 */
public class Main {

    public static void main(String[] args) {
//        try {
//            UIManager.setLookAndFeel(new DarculaLaf());
//        } catch (UnsupportedLookAndFeelException e) {
//            e.printStackTrace();
//        }

        if (Registry.loadUsernameAndPassword().equals(" ")) {
            LoginForm.main(args);
        } else {
            MainForm.main(args);
        }
    }
    
}
