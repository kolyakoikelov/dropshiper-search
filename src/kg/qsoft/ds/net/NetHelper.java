package kg.qsoft.ds.net;

import kg.qsoft.ds.parser.DropGrableyParser;
import kg.qsoft.ds.parser.GrableyParser;
import kg.qsoft.ds.preference.Registry;
import kg.qsoft.ds.ui.CaptchaResolverForm;
import okhttp3.*;
import okhttp3.internal.JavaNetCookieJar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.swing.*;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by akoikelov
 */
public class NetHelper {

    private final String URL_PREFIX = "http://localhost:8000";
    private static NetHelper instance = new NetHelper();
    private OkHttpClient client;
    private Request.Builder builder = new Request.Builder();
    private ArrayList<String> proxies = new ArrayList<>();
    private Random random = new Random();
    private Random randomBuilder = new Random();
    private int proxiesSize;
    private int randomBuilderSize;
    private ArrayList<Request.Builder> builders = new ArrayList<>();
    private boolean captchaSolved = false;

    private NetHelper() {
        CookieHandler cookieHandler = new CookieManager();
        client = new OkHttpClient.Builder().cookieJar(new JavaNetCookieJar(cookieHandler)).build();

        builder.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36")
               .addHeader("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");

        for (int i = 0; i < 5; i++) {
            Request.Builder randomBuilder = new Request.Builder();
            randomBuilder.addHeader("User-Agent", String.format("Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.%s Safari/537.36", i))
                         .addHeader("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            builders.add(randomBuilder);
        }

        proxies.add("http://proxy-us1.toolur.com/includes/process.php?action=update");
        proxies.add("http://proxy-us3.toolur.com/includes/process.php?action=update");
        proxies.add("http://proxy-us4.toolur.com/includes/process.php?action=update");
        proxies.add("http://proxy-us2.toolur.com/includes/process.php?action=update");

        proxiesSize = proxies.size();
        randomBuilderSize = builders.size();
    }

    public static NetHelper getInstance() {
        return instance;
    }

    public String get(String url) {
        Request request = getRequest(url);

        try {
            Response response = client.newCall(request).execute();
            String content = response.body().string();

            if (content.contains("Enter the characters you see below")) {
                captchaSolved = false;
                solveCaptcha();

                return get(url);
            } else {
                return content;
            }
        } catch (IOException e) {
            return get(url);
        }
    }

    public String getViaProxy(String url) {
        RequestBody body = new FormBody.Builder()
                               .add("u", url).build();
        String proxyUrl = proxies.get(random.nextInt(proxiesSize));

        Request request = getPostRequest(proxyUrl, body);
        try {
            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            return getViaProxy(url);
        }
    }

    private synchronized void solveCaptcha() {
        if (!captchaSolved) {
            Request request = getRequest("https://amazon.com/errors/validateCaptcha");
            try {
                Response response = client.newCall(request).execute();
                Document document = Jsoup.parse(response.body().string());

                Elements form = document.getElementsByTag("form");
                Elements pictures = form.get(0).getElementsByTag("img");
                Elements amzn = form.get(0).getElementsByAttributeValue("name", "amzn");
                Elements amznr = form.get(0).getElementsByAttributeValue("name", "amzn-r");
                Elements amznpt = form.get(0).getElementsByAttributeValue("name", "amzn-pt");

                if (pictures.size() > 0) {
                    String pictureUrl = pictures.get(0).attr("src");

                    String characters = CaptchaResolverForm.main(pictureUrl);

                    if (!characters.equals("cancelled")) {
                        String url = "https://amazon.com/errors/validateCaptcha" + String.format("?amzn=%s&amzn-r=%s&field-keywords=%s", amzn.attr("value"), amznr.attr("value"), characters);

                        if (amznpt.size() > 0) {
                            url += "&amzn-pt=" + amznpt.attr("value");
                        }

                        Request request2 = getRequest(url);
                        Response response2 = client.newCall(request2).execute();

                        String content = response2.body().string();
                        if (content.contains("Enter the characters you see below")) {
                            solveCaptcha();
                        } else {
                            captchaSolved = true;
                        }
                    }
                }
            } catch (IOException e) {
                showNoConnectionMessage();
            }
        }
    }

    public boolean auth(String username, String password) {
        RequestBody body = new FormBody.Builder()
                               .add("username", username)
                               .add("password", password)
                               .build();

        Request request = getPostRequest(String.format("%s/ds/auth", URL_PREFIX), body);
        try {
            Response response = client.newCall(request).execute();
            String content = response.body().string();

            return content.contains("true");

        } catch (IOException e) {
            showNoConnectionMessage();
        }

        return false;
    }

    public boolean uploadTradesFromGrabley(String login, String password, JButton importBtn, boolean isOldGrabley) {
        try {
            String content;

            importBtn.setEnabled(false);

            if (isOldGrabley) {
                content = getEbayListingPageContent(login, password, "http://ebay.grabley.net/cms", "http://ebay.grabley.net/listings/?menu=1");
            } else {
                Document document = Jsoup.parse(get("http://drop.grabley.net/listings/"));
                Element form = document.getElementById("loginform");

                if (form != null) {
                    content = getEbayListingPageContent(login, password, form.attr("action"), "http://drop.grabley.net/listings/");
                } else {
                    content = document.body().toString();
                }
            }

            if (content == null) {
                JOptionPane.showMessageDialog(null, "Неверный логин или пароль", "Неверный логин или пароль", JOptionPane.ERROR_MESSAGE);
                importBtn.setEnabled(true);
                return false;
            }

            String trades = isOldGrabley ? GrableyParser.parse(content).toString() : DropGrableyParser.parse(content).toString();
            String[] usernameAndPassword = Registry.loadUsernameAndPassword().split(" ");

            RequestBody uploadBody = new FormBody.Builder()
                                         .add("username", usernameAndPassword[0])
                                         .add("password", usernameAndPassword[1])
                                         .add("trades", trades)
                                         .build();

            Request uploadRequest = getPostRequest(String.format("%s/ds/grabley/import?desktopimporter=1", URL_PREFIX), uploadBody);
            Response uploadResponse = client.newCall(uploadRequest).execute();

            String uploadContent = uploadResponse.body().string();

            if (uploadResponse.isSuccessful() && uploadContent.contains("true")) {
                JOptionPane.showMessageDialog(null, "Товары успешно загружены", "Товары успешно загружены", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Проблемы при загрузке товаров", "Возникли проблемы при загрузке товаров", JOptionPane.ERROR_MESSAGE);
            }

        } catch (IOException e) {
            showNoConnectionMessage();
        }

        importBtn.setEnabled(true);
        return true;
    }

    public boolean uploadTrades(JSONArray trades) {
        String[] usernameAndPassword = Registry.loadUsernameAndPassword().split(" ");
        RequestBody body = new FormBody.Builder()
                                       .add("username", usernameAndPassword[0])
                                       .add("password", usernameAndPassword[1])
                                       .add("trades", trades.toString()).build();

        Request request = getPostRequest(String.format("%s/ds/aggregated/upload/new", URL_PREFIX), body);
        try {
            Response response = client.newCall(request).execute();
            return response.body().string().contains("true");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean uploadPair(JSONObject pair) {
        String[] usernameAndPassword = Registry.loadUsernameAndPassword().split(" ");
        RequestBody body = new FormBody.Builder()
                .add("username", usernameAndPassword[0])
                .add("password", usernameAndPassword[1])
                .add("paired", pair.toString()).build();

        Request request = getPostRequest(String.format("%s/ds/paired/trades/upload", URL_PREFIX), body);
        try {
            Response response = client.newCall(request).execute();
            return response.body().string().contains("true");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    private String getEbayListingPageContent(String login, String password, String loginUrl, String listingUrl) throws IOException {
        RequestBody authBody = new FormBody.Builder()
                .add("log", login)
                .add("pwd", password)
                .add("redirect_to", "http://ebay.grabley.net/")
                .build();
        Request authRequest = getPostRequest(loginUrl, authBody);
        Response authResponse = client.newCall(authRequest).execute();

        if (authResponse.body().string().contains("ОШИБКА")) {
            return null;
        }

        Request listingRequest = getRequest(listingUrl);
        Response listingResponse = client.newCall(listingRequest).execute();

        return listingResponse.body().string();
    }

    private Request getRequest(String url) {
        if (url.contains("walmart")) {
            return builders.get(randomBuilder.nextInt(randomBuilderSize)).url(url)
                    .build();
        } else {
            return builder.url(url).build();
        }
    }

    private Request getPostRequest(String url, RequestBody body) {
        return builder.url(url)
                      .post(body)
                      .build();
    }

    private void showNoConnectionMessage() {
        JOptionPane.showMessageDialog(null, "Не могу отправить запрос. Проверьте подлючение к интернету.", "Не могу отправить запрос. Проверьте подлючение к интернету.", JOptionPane.ERROR_MESSAGE);
    }

}
