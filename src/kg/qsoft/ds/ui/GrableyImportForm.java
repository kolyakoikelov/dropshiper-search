package kg.qsoft.ds.ui;

import kg.qsoft.ds.net.NetHelper;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by akoikelov
 */
public class GrableyImportForm extends JDialog {
    private JTextField usernameText;
    private JPasswordField passwordText;
    private JLabel usernameLbl;
    private JLabel passwordLbl;
    private JPanel rootPane;
    private JButton importBtn;
    private JLabel informationLbl;
    private static JDialog frame;
    private static boolean isOldGrableyValue;

    static void main(JFrame parent, boolean isOldGrabley) {
        isOldGrableyValue = isOldGrabley;
        String title = isOldGrabley ? "Импорт из ebay.grabley.net" : "Импорт из drop.grabley.net";

        frame = new JDialog();
        frame.setContentPane(new GrableyImportForm().rootPane);
        frame.setTitle(title);
        frame.setResizable(false);
        frame.setModal(true);
        frame.pack();
        frame.setLocationRelativeTo(parent);
        frame.setVisible(true);
    }

    private void createUIComponents() {
        importBtn = new JButton();
        importBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String login = usernameText.getText();
                String password = String.valueOf(passwordText.getPassword());

                NetHelper.getInstance().uploadTradesFromGrabley(login, password, importBtn, isOldGrableyValue);
            }
        });
    }
}
