package kg.qsoft.ds.ui;

import kg.qsoft.ds.parser.amazon.AmazonSpyParser;
import kg.qsoft.ds.parser.amazon.AmazonUrlParser;
import kg.qsoft.ds.parser.ebay.EbayParser;
import kg.qsoft.ds.parser.walmart.WalmartParser;
import kg.qsoft.ds.preference.Registry;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.io.InterruptedIOException;

/**
 * Created by akoikelov
 */
public class MainForm {
    private JPanel rootPane;
    private JTextField urlText;
    private JButton submitBtn;
    private JPanel searchPane;
    private JLabel enterUrlLbl;
    private JTable resultTable;
    private DefaultTableModel tableModel;
    private JPanel resultPane;
    private JScrollPane tableScrollPane;
    private JProgressBar loadingProgress;
    private JButton stopBtn;
    private JLabel uploadedLbl;
    private JLabel spyUploadedLbl;
    private JComboBox shopsBox;
    private JButton clearBtn;
    private JLabel pairedUploadedLbl;
    private JProgressBar pairedUploadedProgress;
    private JCheckBox filterSellingTrades;
    private JCheckBox onlyPairedTrades;
    private static JMenuBar menuBar;
    private static JMenu menuFile;
    private static JMenu menuHelp;
    private static JMenuItem changeUserMenuItem;
    private static JMenuItem aboutMenuItem;
    private static JMenuItem importGrableyMenuItem;
    private static JMenuItem importDropGrableyMenuItem;
    private int spyUploadedQuantity = 0;
    private final static JFrame frame = new JFrame("Dropshiper Search");

    private Thread urlParserThread;

    public MainForm() {

    }

    public static void main(final String[] args) {
        frame.setContentPane(new MainForm().rootPane);
        frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                if (createConfirmDialog("Подтверждение выхода", "Вы действительно хотите выйти?")) {
                    System.exit(0);
                }
            }
        });

        ImageIcon icon = new ImageIcon(frame.getClass().getResource("/app.png"));

        frame.setIconImage(icon.getImage());
        frame.setResizable(false);
        frame.pack();

        menuBar = new JMenuBar();
        menuFile = new JMenu("Файл");
        menuHelp = new JMenu("Помощь");
        changeUserMenuItem = new JMenuItem("Войти под другим пользователем");
        aboutMenuItem = new JMenuItem("О программе");
        importGrableyMenuItem = new JMenuItem("Импорт из ebay.grabley.net");
        importDropGrableyMenuItem = new JMenuItem("Импорт из drop.grabley.net");

        importGrableyMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GrableyImportForm.main(frame, true);
            }
        });
        importDropGrableyMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GrableyImportForm.main(frame, false);
            }
        });

        changeUserMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (createConfirmDialog("Подтверждение действия", "Вы уверены?")) {
                    Registry.saveUsernameAndPassword("", "");
                    frame.dispose();
                    LoginForm.main(args);
                }
            }
        });

        aboutMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(frame, "<HTML><p>Версия: 1.3</p> <br> <p>Dropshiper Search. 2016-2017</p> <p>http://dropshiper.pro<p></HTML>", "О программе", JOptionPane.INFORMATION_MESSAGE);
            }
        });

        menuFile.add(changeUserMenuItem);
        menuFile.add(importGrableyMenuItem);
        menuFile.add(importDropGrableyMenuItem);
        menuHelp.add(aboutMenuItem);
        menuBar.add(menuFile);
        menuBar.add(menuHelp);

        frame.setJMenuBar(menuBar);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    protected void createUIComponents() {
        String[] amazonColumnNames = new String[]{
                "ASIN", "Title", "Price", "Offers", "Rating",
                "BSR", "Category", "Brand", "Buybox", "Currently Unavailable", "Prime Seller", "Action"
        };
        String[] eBayColumnNames = new String[]{
                "ID", "Title", "Price", "Shipping", "Feedback score", "Positive feedback", "Fast 'n free", "UPC", "Sold"
        };
        String[] walmartColumnNames = new String[]{
                "ID", "Title", "Price", "Brand", "Rating", "UPC", "Shipping"
        };
        String[] shops = new String[]{
                "Amazon", "eBay", "Walmart"
        };

        final DefaultTableModel amazonModel = new DefaultTableModel(0, amazonColumnNames.length);
        final DefaultTableModel ebayModel = new DefaultTableModel(0, eBayColumnNames.length);
        final DefaultTableModel walmartModel = new DefaultTableModel(0, walmartColumnNames.length);

        amazonModel.setColumnIdentifiers(amazonColumnNames);
        ebayModel.setColumnIdentifiers(eBayColumnNames);
        walmartModel.setColumnIdentifiers(walmartColumnNames);

        this.resultTable = new JTable(amazonModel);
        this.tableModel = (DefaultTableModel) this.resultTable.getModel();
        this.submitBtn = new JButton();
        this.stopBtn = new JButton();
        this.urlText = new JTextField();
        this.resultTable.setEnabled(false);
        this.loadingProgress = new JProgressBar();
        this.shopsBox = new JComboBox<>(shops);
        this.clearBtn = new JButton();
        this.pairedUploadedProgress = new JProgressBar();
        this.pairedUploadedLbl = new JLabel();
        this.filterSellingTrades = new JCheckBox();
        this.onlyPairedTrades = new JCheckBox();

        this.shopsBox.setEditable(false);
        this.loadingProgress.setMaximum(500);
        this.pairedUploadedProgress.setMaximum(1000);

        this.clearBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (createConfirmDialog("Очистить результат", "Вы действительно хотите очистить результат?")) {
                    ((DefaultTableModel) resultTable.getModel()).setRowCount(0);
                    urlText.setText("");
                }
            }
        });

        this.submitBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                shopsBox.setEnabled(false);
                loadingProgress.setValue(0);
                urlParserThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String shop = shopsBox.getSelectedItem().toString();
                            submitBtn.setEnabled(false);

                            if (shop.equals("Amazon")) {
                                amazonModel.setRowCount(0);
                                AmazonUrlParser.parse(urlText.getText(), 1, loadingProgress, resultTable, uploadedLbl, 0);
                            } else if (shop.equals("eBay")) {
                                ebayModel.setRowCount(0);
                                EbayParser.parse(urlText.getText(), 1, loadingProgress, resultTable, uploadedLbl, 0, pairedUploadedLbl,
                                                 pairedUploadedProgress, filterSellingTrades, onlyPairedTrades);
                            } else if (shop.equals("Walmart")) {
                                walmartModel.setRowCount(0);
                                WalmartParser.parse(urlText.getText(), 1, loadingProgress, resultTable, uploadedLbl, 0, pairedUploadedLbl, pairedUploadedProgress, onlyPairedTrades, -1);
                            }

                            shopsBox.setEnabled(true);
                            submitBtn.setEnabled(true);
                        } catch (InterruptedIOException e1) {
                            e1.printStackTrace();
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                            submitBtn.setEnabled(true);
                            shopsBox.setEnabled(true);
                            JOptionPane.showMessageDialog(frame, "Неверный URL", "Неверный URL", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                });

                urlParserThread.start();
            }
        });

        this.stopBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (createConfirmDialog("Остановка процесса", "Вы действительно хотите остановить?")) {
                    if (urlParserThread != null) {
                        urlParserThread.stop();
                        shopsBox.setEnabled(true);
                        submitBtn.setEnabled(true);
                        uploadedLbl.setText("Загружено: 0");
                        spyUploadedLbl.setText("Товаров по шпиону: 0");
                        pairedUploadedLbl.setText("Вилки: 0");
                        pairedUploadedProgress.setValue(0);
                    }
                }
            }
        });

        this.resultTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent e) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int row = resultTable.rowAtPoint(e.getPoint());
                        int col = resultTable.columnAtPoint(e.getPoint());

                        if (col == 11) {
                            String value = (String) tableModel.getValueAt(row, col);

                            if (!value.equals("OK")) {
                                tableModel.setValueAt("OK", row, col);
                                String asin = (String) tableModel.getValueAt(row, 0);
                                spyUploadedQuantity += AmazonSpyParser.parse(asin, spyUploadedLbl, spyUploadedQuantity);
                            }
                        }
                    }
                }).start();
            }
        });

        this.shopsBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    String shop = e.getItem().toString();

                    if (shop.equals("Amazon")) {
                        resultTable.setModel(amazonModel);
                        filterSellingTrades.setVisible(false);
                        onlyPairedTrades.setVisible(false);
                    } else if (shop.equals("eBay")) {
                        resultTable.setModel(ebayModel);
                        filterSellingTrades.setVisible(true);
                        onlyPairedTrades.setVisible(true);
                    } else if (shop.equals("Walmart")){
                        resultTable.setModel(walmartModel);
                        filterSellingTrades.setVisible(false);
                        onlyPairedTrades.setVisible(true);
                    }
                }
            }
        });
    }

    private static boolean createConfirmDialog(String title, String text) {
        String buttons[] = {"Да", "Нет"};
        int prompt = JOptionPane.showOptionDialog(null, text, title, JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, buttons, buttons[1]);

        return prompt == JOptionPane.YES_OPTION;
    }
}
