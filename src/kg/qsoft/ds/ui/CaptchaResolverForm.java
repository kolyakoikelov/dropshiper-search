package kg.qsoft.ds.ui;

import javax.swing.*;
import java.awt.event.*;
import java.net.MalformedURLException;
import java.net.URL;

public class CaptchaResolverForm extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField captchaText;
    private JLabel captchaImage;
    private String pictureUrl;
    private String answer;

    public CaptchaResolverForm(String pictureUrl) {
        this.pictureUrl = pictureUrl;

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        answer = captchaText.getText();
        dispose();
    }

    private void onCancel() {
        answer = "cancelled";
        dispose();
    }

    public String getAnswer() {
        return answer;
    }

    public static String main(String pictureUrl) {
        CaptchaResolverForm dialog = new CaptchaResolverForm(pictureUrl);
        dialog.setTitle("Ввод капчи");
        dialog.pack();
        dialog.setLocationRelativeTo(null);
        dialog.setResizable(false);
        dialog.setVisible(true);

        return dialog.getAnswer();
    }

    private void createUIComponents() {
        captchaImage = new JLabel();
        try {
            captchaImage.setIcon(new ImageIcon(new URL(pictureUrl)));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
