package kg.qsoft.ds.ui;

import kg.qsoft.ds.net.NetHelper;
import kg.qsoft.ds.preference.Registry;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by akoikelov
 */
public class LoginForm {
    private JTextField usernameText;
    private JPasswordField passwordText;
    private JLabel usernameLbl;
    private JLabel passwordLbl;
    private JPanel rootPane;
    private JButton loginBtn;
    private JLabel informationLbl;
    private NetHelper netHelper = NetHelper.getInstance();
    private static JFrame frame;

    public LoginForm() {

    }

    public static void main(String[] args) {
        frame = new JFrame();
        frame.setContentPane(new LoginForm().rootPane);
        frame.setTitle("Авторизация");
        frame.setResizable(false);

        ImageIcon icon = new ImageIcon(frame.getClass().getResource("/app.png"));

        frame.setIconImage(icon.getImage());
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();

        frame.setLocationRelativeTo(null);

        frame.setVisible(true);
    }

    private void createUIComponents() {
        loginBtn = new JButton();
        loginBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String username = usernameText.getText();
                char[] password = passwordText.getPassword();

                boolean isRight = netHelper.auth(username, String.valueOf(password));

                if (isRight) {
                    Registry.saveUsernameAndPassword(username, String.valueOf(password));
                    frame.dispose();

                    MainForm.main(new String[] {});
                } else {
                    informationLbl.setText("Неверный логин или пароль.");
                    informationLbl.setVisible(true);
                }
            }
        });
    }

}
