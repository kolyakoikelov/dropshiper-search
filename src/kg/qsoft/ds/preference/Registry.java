package kg.qsoft.ds.preference;

import java.util.prefs.Preferences;

/**
 * Created by akoikelov
 */
public class Registry {

    public static void saveUsernameAndPassword(String username, String password) {
        Preferences pref = Preferences.systemRoot();
        pref.put("username", username);
        pref.put("password", password);
    }

    public static String loadUsernameAndPassword() {
        Preferences pref = Preferences.systemRoot();
        String username = pref.get("username", "");
        String password = pref.get("password", "");

        return String.format("%s %s", username, password);
    }

}
