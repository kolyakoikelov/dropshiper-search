package kg.qsoft.ds.parser.page;

import kg.qsoft.ds.parser.AbstractParser;
import org.json.simple.JSONObject;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by akoikelov
 */
public class WalmartTradePageParser extends AbstractParser {

    public static JSONObject parse(String url) {
        Document doc = getPageDocument(url);
        JSONObject trade = new JSONObject();

        try {
            Elements title = doc.getElementsByClass("prod-ProductTitle");
            Elements price = doc.select("span.Price--stylized");
            Elements brand = doc.select("span[itemprop=brand]");
            Elements rating = doc.select("span.stars-container");
            Elements shipping = doc.getElementsByClass("prod-shippingMessage");
            Elements provider = doc.getElementsByClass("prod-SoldShipByMsg");
            Elements picture = doc.select("img.prod-HeroImage-image");
            String titleValue = "-";
            String priceValue = "0";
            String brandValue = "-";
            String ratingValue = "0";
            String upcValue = "-";
            String shippingValue = "0";
            String providerValue = "-";
            Pattern numberPattern = Pattern.compile("([0-9.]+)");
            Pattern upcPattern = Pattern.compile("\"upc\":\"([0-9]+)\"");
            Pattern shippingPattern = Pattern.compile("ship for \\$([0-9.]+)");
            Pattern shippingPattern2 = Pattern.compile("\\$([0-9.]+) shipping");

            if (title.size() > 0) {
                titleValue = title.text();
            }

            if (titleValue.equals("-")) {
                return null;
            }

            if (price.size() > 0) {
                Matcher priceMatcher = numberPattern.matcher(price.get(0).text());

                if (priceMatcher.find()) {
                    priceValue = priceMatcher.group(0);
                } else {
                    priceValue = "0";
                }
            }
            if (brand.size() > 0) {
                brandValue = brand.get(0).text();
            }
            if (rating.size() > 0) {
                Matcher ratingMatcher = numberPattern.matcher(rating.get(0).attr("alt"));

                if (ratingMatcher.find()) {
                    ratingValue = ratingMatcher.group(0);
                }
            }

            if (shipping.size() > 0) {
                Matcher shippingMatcher = shippingPattern.matcher(shipping.get(0).text());
                Matcher shippingMatcher2 = shippingPattern2.matcher(shipping.get(0).text());

                if (shippingMatcher.find()) {
                    shippingValue = shippingMatcher.group(1);
                } else if (shippingMatcher2.find()) {
                    shippingValue = shippingMatcher2.group(1);
                }
            }

            Matcher upcMatcher = upcPattern.matcher(doc.html());

            if (upcMatcher.find()) {
                upcValue = upcMatcher.group(1);
            }

            if (provider.size() > 0) {
                providerValue = provider.get(0).text();
            }

            if (picture.size() > 0) {
                trade.put("picture", picture.attr("src"));
            } else {
                trade.put("picture", "-");
            }

            trade.put("title", titleValue);
            trade.put("url", url);
            trade.put("price", priceValue);
            trade.put("brand", brandValue);
            trade.put("rating", ratingValue);
            trade.put("shop", "walmart");
            trade.put("sold_score", "0");
            trade.put("has_prime_in_sellers", false);
            trade.put("upc", upcValue);
            trade.put("shipping", shippingValue);
            trade.put("provider", providerValue);

            return trade;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

}
