package kg.qsoft.ds.parser.page;

import kg.qsoft.ds.parser.AbstractParser;
import org.json.simple.JSONObject;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by akoikelov
 */
public class EbayTradePageParser extends AbstractParser {

    public static JSONObject parse(String url) {
        Document doc = getPageDocument(url);
        JSONObject trade = new JSONObject();

        try {
            Element title = doc.getElementById("itemTitle");
            Element price = doc.getElementById("prcIsum");
            Element positiveFeedback = doc.getElementById("si-fb");
            Elements feedbackScore = doc.getElementsByClass("mbg-l");
            Element shipping = doc.getElementById("fshippingCost");
            Element upc = doc.select("td:contains(UPC:)").first();
            Element picture = doc.getElementById("icImg");
            Elements soldScore = doc.select("span.vi-qtyS-hot-red");
            Elements provider = doc.getElementsByClass("mbg-nw");

            String positiveFeedbackVal = "0";
            String feedbackScoreVal = "0";
            String priceVal = "0";
            String shippingVal = "0";
            String upcVal = "-";
            String soldScoreVal = "0";
            String providerVal = "-";
            Pattern numberPattern = Pattern.compile("([0-9.]+)");
            Pattern soldScorePatten = Pattern.compile("([0-9,]+) sold");

            if (price != null) {
                priceVal = price.attr("content");
            } else {
                price = doc.getElementById("mm-saleDscPrc");
                if (price != null) {
                    Matcher priceMatcher = numberPattern.matcher(price.text());

                    if (priceMatcher.find()) {
                        priceVal = priceMatcher.group(0);
                    }
                }
            }

            if (soldScore.size() > 0) {
                Matcher soldScoreMatcher = soldScorePatten.matcher(soldScore.text());

                if (soldScoreMatcher.find()) {
                    soldScoreVal = soldScoreMatcher.group(1).replace(",", "");
                }
            }

            if (positiveFeedback != null) {
                Matcher feedbackMatcher = numberPattern.matcher(positiveFeedback.text());

                if (feedbackMatcher.find()) {
                    positiveFeedbackVal = feedbackMatcher.group(0);
                }
            }

            if (feedbackScore.size() > 0) {
                feedbackScoreVal = feedbackScore.get(0).getElementsByTag("a").text();
            }

            if (shipping != null) {
                Matcher shippingMatcher = numberPattern.matcher(shipping.text());

                if (shippingMatcher.find()) {
                    shippingVal = shippingMatcher.group(0);
                }
            }

            if (upc != null) {
                Element upcSibling = upc.nextElementSibling();
                if (upcSibling  != null) {
                    Matcher upcMatcher = numberPattern.matcher(upcSibling.text());
                    if (upcMatcher.find()) {
                        upcVal = upcMatcher.group(0);
                    }
                }
            }

            if (provider.size() > 0) {
                providerVal = provider.get(0).text();
            }

            trade.put("title", title.ownText());
            trade.put("url", url);
            trade.put("price", priceVal);
            trade.put("feedback_score", feedbackScoreVal);
            trade.put("positive_feedback", positiveFeedbackVal);
            trade.put("shipping", shippingVal);
            trade.put("upc", upcVal);
            trade.put("shop", "ebay");
            trade.put("sold_score", soldScoreVal);
            trade.put("provider", providerVal);
            trade.put("has_prime_in_sellers", false);

            if (picture != null) {
                trade.put("picture", picture.attr("src"));
            } else {
                trade.put("picture", "-");
            }

            return trade;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

}
