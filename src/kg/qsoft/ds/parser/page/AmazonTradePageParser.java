package kg.qsoft.ds.parser.page;

import kg.qsoft.ds.parser.AbstractParser;
import org.json.simple.JSONObject;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by akoikelov
 */
public class AmazonTradePageParser extends AbstractParser {

    public static JSONObject parse(String url, String asin) {
        Document document = getPageDocument(url);
        JSONObject data = new JSONObject();

        try {
            String title = document.getElementById("productTitle").text();
            float price = 0;
            String offers = "1";
            String bsr = "0";
            Boolean hasBuybox = true;
            Boolean inStock = true;
            String bsrCategory = "-";
            String brand = "-";

            Element priceEl = document.getElementById("priceblock_ourprice");
            Element salePrice = document.getElementById("priceblock_saleprice");
            Element shipping = document.getElementById("ourprice_shippingmessage");
            Element salesRankEl = document.getElementById("SalesRank");
            Element seeAllOptions = document.getElementById("buybox-see-all-buying-choices-announce");
            Element brandInfo = document.getElementById("bylineInfo_feature_div");

            String offersRegex = "[nN]ew \\(([0-9]+)\\)";
            String offersRegexSecond = "([0-9]+)&nbsp;[nN]ew";
            String salesRankRegex = "#([0-9\\,]+) in ([^\\(>]+)";
            String shippingRegex = "([0-9.]+) shipping";
            String lowestPriceRegex = "from \\$([0-9.]+)";
            String numberRegex = "([0-9.]+)";
            Pattern offersPattern = Pattern.compile(offersRegex);
            Matcher offersMatcher = offersPattern.matcher(document.text());
            Pattern shippingPattern = Pattern.compile(shippingRegex);
            Pattern lowestPricePattern = Pattern.compile(lowestPriceRegex);
            Pattern numberPattern = Pattern.compile(numberRegex);

            if (offersMatcher.find()) {
                offers = offersMatcher.group(1);
            } else {
                Pattern offersSecondPattern = Pattern.compile(offersRegexSecond);
                Matcher offersSecondMatcher = offersSecondPattern.matcher(document.html());

                if (offersSecondMatcher.find()) {
                    offers = offersSecondMatcher.group(1);
                }
            }

            if (brandInfo != null) {
                brand = brandInfo.text().replace("by ", "");
            }
            if (seeAllOptions != null) {
                hasBuybox = false;

                Element priceBlock = document.getElementById("olp_feature_div");

                if (priceBlock == null) {
                    priceBlock = document.getElementById("olpDiv");
                }

                if (priceBlock != null) {
                    Matcher lowestPriceMatcher = lowestPricePattern.matcher(priceBlock.text());
                    if (lowestPriceMatcher.find()) {
                        price = Float.valueOf(lowestPriceMatcher.group(1).replace(",", ""));
                        Matcher shippingMatcher = shippingPattern.matcher(priceBlock.text());

                        if (shippingMatcher.find()) {
                            price += Float.valueOf(shippingMatcher.group(1).replace(",", ""));
                        }
                    }
                }
            }
            if (salesRankEl == null) {
                salesRankEl = document.getElementById("productDetails_detailBullets_sections1");
            }

            if (salesRankEl != null) {
                Pattern srPattern = Pattern.compile(salesRankRegex);
                Matcher srMatcher = srPattern.matcher(salesRankEl.text());

                if (srMatcher.find()) {
                    bsrCategory = srMatcher.group(2).trim();
                    if (srMatcher.group(1) != null) {
                        bsr = srMatcher.group(1).replace(",", "");
                    }
                }
            }

            String priceValue = null;

            if (priceEl != null) {
                priceValue = priceEl.text().replace(",", "");
            } else if (salePrice != null) {
                priceValue = salePrice.text().replace(",", "");
            }

            if (priceValue != null) {
                Matcher priceMatcher = numberPattern.matcher(priceValue);

                if (priceMatcher.find()) {
                    price = Float.valueOf(priceMatcher.group(0));
                }
            }

            if (shipping != null) {
                Matcher shippingMatcher = shippingPattern.matcher(shipping.text());

                if (shippingMatcher.find()) {
                    price += Float.valueOf(shippingMatcher.group(1).replace(",", ""));
                }
            }

            if (document.text().contains("Currently unavailable.")) {
                inStock = false;
                hasBuybox = false;
            }

            boolean hasPrime = checkHasPrime(asin);

            if (!inStock) {
                hasPrime = false;
            }

            data.put("title", title);
            data.put("price", ((int)(price * 100)) / 100.0);
            data.put("url", url);
            data.put("offers", offers);
            data.put("bsr", bsr);
            data.put("has_buybox", hasBuybox);
            data.put("bsr_category", bsrCategory);
            data.put("brand", brand);
            data.put("in_stock", inStock);
            data.put("shop", "amazon");
            data.put("sold_score", "0");
            data.put("has_prime_in_sellers", hasPrime);

            return data;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }

    private static boolean checkHasPrime(String asin) {
        Document document = getPageDocument(String.format(AMAZON_OFFERS_URL, asin));
        return document.select("i.a-icon-prime").size() > 0;
    }

}
