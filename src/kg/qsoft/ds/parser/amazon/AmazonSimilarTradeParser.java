package kg.qsoft.ds.parser.amazon;

import kg.qsoft.ds.parser.AbstractSimilarTradeParser;
import kg.qsoft.ds.parser.page.AmazonTradePageParser;
import org.json.simple.JSONObject;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by akoikelov
 */
public class AmazonSimilarTradeParser extends AbstractSimilarTradeParser {

    public static JSONObject parseSimilarTrade(String upc) {
        String currentUrl = String.format(AMAZON_SEARCH_URL, upc);

        Document document = getPageDocument(currentUrl);
        Elements tradesList = document.getElementsByClass("s-result-item");

        if (tradesList.size() > 0) {
            Element item = tradesList.get(0);
            Elements prime = item.getElementsByClass("a-icon-prime");

            if (prime.size() == 0) {
                String urlToTrade = String.format(AMAZON_TRADE_URL, item.attr("data-asin"));
                JSONObject trade = AmazonTradePageParser.parse(urlToTrade, item.attr("data-asin"));

                if (trade != null) {
                    Elements rating = item.getElementsByClass("a-icon-star");
                    Elements picture = item.getElementsByClass("s-access-image");

                    if (rating.size() > 0) {
                        trade.put("rating", rating.text().split("\\s")[0]);
                    } else {
                        trade.put("rating", "0");
                    }

                    if (picture.size() > 0) {
                        trade.put("picture", picture.get(0).attr("src"));
                    } else {
                        trade.put("picture", "-");
                    }

                    trade.put("trade_id", item.attr("data-asin"));
                    trade.put("spy_trade", false);

                    return trade;
                }
            }
        }

        return null;
    }

    private static ArrayList<Float> parseLowestPrice(String asin, int page) {
        asin = asin.trim();

        ArrayList<Float> prices = new ArrayList<>();
        String prefix = "";
        float priceValue;
        float shippingValue;

        if (page >= 1) {
            prefix += String.format("&startIndex=%s", page * 10);
        }

        Document document = getPageDocument(String.format(AMAZON_OFFERS_URL, asin) + prefix);
        Elements sellers = document.getElementsByClass("olpOffer");

        for (Element seller: sellers) {
            Elements price = seller.getElementsByClass("olpOfferPrice");
            Elements shipping = seller.getElementsByClass("olpShippingPrice");

            if (price.size() > 0) {
                priceValue = Float.valueOf(price.get(0).text().replace("$", "").replace(",", "").trim());

                if (shipping.size() > 0) {
                    shippingValue = Float.valueOf(shipping.get(0).text().replace("$", "").replace(",", "").trim());
                    priceValue += shippingValue;
                }

                prices.add((float) (Math.round(priceValue * 100.0) / 100.0));
            }
        }

        Elements pagination = document.getElementsByClass("a-pagination");

        if (pagination.size() == 0) {
            return prices;
        }

        Elements paginationLastItem = pagination.get(0).getElementsByClass("a-last");

        if (paginationLastItem.size() > 0) {
            Elements link = paginationLastItem.get(0).getElementsByTag("a");

            if (link.size() > 0) {
                page++;
                ArrayList<Float> recursionPrices = parseLowestPrice(asin, page);

                for (float price : recursionPrices) {
                    prices.add(price);
                }

                return prices;
            }
        }

        return prices;
    }

}
