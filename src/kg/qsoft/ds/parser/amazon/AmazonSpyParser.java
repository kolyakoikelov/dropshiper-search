package kg.qsoft.ds.parser.amazon;

import kg.qsoft.ds.parser.AbstractParser;
import kg.qsoft.ds.parser.page.AmazonTradePageParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.swing.*;

/**
 * Created by akoikelov
 */
public class AmazonSpyParser extends AbstractParser {

    public static int parse(String asin, JLabel spyUploadedLbl, int mSpyUploadedQuantity) {
        final JSONArray trades = new JSONArray();

        Document document = getPageDocument(String.format(AMAZON_OFFERS_URL, asin));
        Elements sellers = document.getElementsByClass("olpSellerName");

        for (Element seller: sellers) {
            Elements links = seller.getElementsByTag("a");

            if (links.size() > 0) {
                String marketHtml = helper.get(AMAZON_URL + links.get(0).attr("href"));
                Document marketDocument = Jsoup.parse(marketHtml);
                Elements topTrades = marketDocument.getElementsByClass("a-carousel-card");

                for (Element item: topTrades) {
                    Elements linksToTrade = item.getElementsByTag("a");

                    if (linksToTrade.size() > 0) {
                        Elements picture = item.select("div.product-image > a > img");
                        String[] parts = linksToTrade.get(0).attr("href").split("/");
                        String itemAsin = parts[parts.length - 1].split("\\?")[0];
                        Elements icons = item.getElementsByClass("a-icon-alt");
                        Elements ratings = item.getElementsByClass("product-stars");

                        if (icons.size() > 0 && icons.text().equals("Prime")) {
                            continue;
                        }

                        JSONObject trade = AmazonTradePageParser.parse(AMAZON_URL + linksToTrade.get(0).attr("href"), itemAsin);

                        if (trade == null) {
                            continue;
                        }

                        if (picture.size() > 0) {
                            trade.put("picture", picture.get(0).attr("src"));
                        } else {
                            trade.put("picture", "-");
                        }

                        trade.put("trade_id", itemAsin);
                        trade.put("spy_trade", true);

                        if (ratings.size() > 0) {
                            trade.put("rating", ratings.get(0).text().split(" ")[0]);
                        } else {
                            trade.put("rating", "0");
                        }

                        trades.add(trade);

                    }
                }

                mSpyUploadedQuantity += trades.size();
                spyUploadedLbl.setText(String.format("Товаров по шпиону загружено: %s", mSpyUploadedQuantity));
                uploadTrades(trades);
            }
        }

        return mSpyUploadedQuantity;
    }

}
