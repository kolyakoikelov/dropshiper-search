package kg.qsoft.ds.parser.amazon;

import kg.qsoft.ds.parser.AbstractParser;
import kg.qsoft.ds.parser.page.AmazonTradePageParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.io.InterruptedIOException;

/**
 * Created by akoikelov
 */
public class AmazonUrlParser extends AbstractParser {

    public static boolean parse(String url, int page, JProgressBar progress, JTable resultTable, JLabel uploadedLbl, int quantity) throws InterruptedIOException {
        DefaultTableModel model = (DefaultTableModel) resultTable.getModel();
        final JSONArray trades = new JSONArray();

        Document document = getPageDocument(url + "&page=" + page);
        Elements tradesList = document.getElementsByClass("s-result-item");

        for (Element item : tradesList) {
            Elements prime = item.getElementsByClass("a-icon-prime");

            if (prime.size() == 0) {
                String urlToTrade = String.format(AMAZON_TRADE_URL, item.attr("data-asin"));
                JSONObject trade = AmazonTradePageParser.parse(urlToTrade, item.attr("data-asin"));

                if (trade == null) {
                    continue;
                }

                Elements rating = item.getElementsByClass("a-icon-star");
                Elements picture = item.getElementsByClass("s-access-image");

                if (rating.size() > 0) {
                    trade.put("rating", rating.text().split("\\s")[0]);
                } else {
                    trade.put("rating", "0");
                }

                if (picture.size() > 0) {
                    trade.put("picture", picture.get(0).attr("src"));
                } else {
                    trade.put("picture", "-");
                }

                trade.put("trade_id", item.attr("data-asin"));
                trade.put("spy_trade", false);

                trades.add(trade);

                model.addRow(new Object[] {
                        trade.get("trade_id"), trade.get("title"),
                        trade.get("price"), trade.get("offers"),
                        trade.get("rating"), trade.get("bsr"),
                        trade.get("bsr_category"), trade.get("brand"),
                        (Boolean) trade.get("has_buybox") ? "yes" : "no", (Boolean) trade.get("in_stock") ? "in stock": "out of stock",
                        (Boolean) trade.get("has_prime_in_sellers") ? "yes" : "no", "Spy"
                });
            }
        }

        quantity += trades.size();
        setUploadedText(uploadedLbl, quantity);

        uploadTrades(trades);

        Element nextLink = document.getElementById("pagnNextLink");

        if (nextLink != null) {
            progress.setValue(page);
            page++;
            return parse(url, page, progress, resultTable, uploadedLbl, quantity);
        } else {
            progress.setValue(500);
            return true;
        }
    }

}
