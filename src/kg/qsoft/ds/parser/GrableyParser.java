package kg.qsoft.ds.parser;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by akoikelov
 */
public class GrableyParser {

    public static JSONArray parse(String content) {
        Document doc = Jsoup.parse(content);
        JSONArray trades = new JSONArray();

        Element table = doc.getElementById("table_listings");
        Elements trs = table.getElementsByTag("tr");

        for (Element tr: trs) {
            Elements tds = tr.getElementsByTag("td");

            if (tds.size() > 0) {
                Element amazonTd = tds.get(4);
                Element providerTd = tds.get(6);
                Element shopTd = tds.get(12);
                Element amazonPriceTd = tds.get(5);
                Element providerPriceTd = tds.get(8);
                Element pictureTd = tds.get(3);
                Element urlTd = tds.get(2);
                Element shopIdTd = tds.get(6);

                try {
                    String asin = amazonTd.getElementsByTag("a").text();
                    String providerId = providerTd.getElementsByTag("input").attr("value");
                    String shop = shopTd.text();
                    String amazonPrice = amazonPriceTd.getElementsByClass("input_price").attr("value");
                    String providerPrice = providerPriceTd.getElementsByTag("input").attr("value");
                    String picture = pictureTd.getElementsByTag("img").attr("src");
                    String title = urlTd.getElementsByTag("a").text();
                    String url = "";

                    if (shop.equals("ebay")) {
                        url = String.format("http://www.ebay.com/itm/%s", shopIdTd.getElementsByTag("input").attr("value"));
                    } else if (shop.equals("walmart")) {
                        url = String.format("http://www.walmart.com/ip/%s", shopIdTd.getElementsByTag("input").attr("value"));
                    }

                    JSONObject trade = new JSONObject();
                    trade.put("asin", asin);
                    trade.put("provider_id", providerId);
                    trade.put("shop", shop);
                    trade.put("amazon_price", amazonPrice);
                    trade.put("provider_price", providerPrice);
                    trade.put("picture", picture);
                    trade.put("title", title);
                    trade.put("url", url);

                    trades.add(trade);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }

        return trades;
    }

}
