package kg.qsoft.ds.parser;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by akoikelov
 */
public class DropGrableyParser {

    public static JSONArray parse(String content) {
        Document doc = Jsoup.parse(content);
        JSONArray trades = new JSONArray();

        Elements rows = doc.getElementsByClass("item-line");

        for (Element row : rows) {
            Elements providers = row.getElementsByClass("item-sup");

            if (providers.size() > 0) {
                Element provider = providers.get(0);
                Elements providerLink = provider.select("div.item-sup-title a");

                String asin = row.getElementsByClass("item-sale-kod").text();
                String providerId = provider.getElementsByClass("item-sup-kod").text();
                String shop = provider.getElementsByClass("item-sup-shop").text().contains("Ebay") ? "ebay" : "walmart";
                String amazonPrice = row.getElementsByClass("item-sale-price").text().replace(",", "").replace("$", "");
                String providerPrice = provider.getElementsByClass("item-sup-price").text().replace(",", "").replace("$", "");
                String picture = provider.select("div.item-sup-photo img").attr("src");

                JSONObject trade = new JSONObject();
                trade.put("asin", asin);
                trade.put("provider_id", providerId);
                trade.put("shop", shop);
                trade.put("amazon_price", amazonPrice);
                trade.put("provider_price", providerPrice);
                trade.put("picture", picture);
                trade.put("title", providerLink.text());
                trade.put("url", providerLink.attr("href"));

                trades.add(trade);
            }
        }

        return trades;
    }

}
