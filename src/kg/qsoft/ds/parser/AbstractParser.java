package kg.qsoft.ds.parser;

import kg.qsoft.ds.net.NetHelper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.swing.*;

/**
 * Created by akoikelov
 */
abstract public class AbstractParser {

    protected static final String AMAZON_OFFERS_URL = "https://www.amazon.com/gp/offer-listing/%s/ref=olpOffersSuppressed?ie=UTF8&overridePriceSuppression=1";
    protected static final String AMAZON_URL = "https://www.amazon.com";
    protected static final String AMAZON_TRADE_URL = "https://www.amazon.com/dp/%s";
    protected static final String EBAY_TRADE_URL = "http://ebay.com/itm/%s";
    protected static final String WALMART_URL = "http://walmart.com";
    protected static final String WALMART_TRADE_URL = "http://walmart.com/ip/%s";
    protected static final String AMAZON_SEARCH_URL = "https://www.amazon.com/s/ref=nb_sb_noss?field-keywords=%s";
    protected static final String EBAY_SEARCH_URL = "http://www.ebay.com/sch/i.html?_nkw=%s";
    protected static final String WALMART_SEARCH_URL = "https://www.walmart.com/search/?query=%s";

    protected static NetHelper helper = NetHelper.getInstance();

    protected static void setUploadedText(JLabel uploadedLbl, int quantity) {
        uploadedLbl.setText(String.format("Загружено: %s", quantity));
    }

    protected static void setPairUploadedText(JLabel pairedUploadedLbl, int quantity) {
        pairedUploadedLbl.setText(String.format("Вилки: %s", quantity));
    }

    protected static void uploadTrades(final JSONArray trades) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                helper.uploadTrades(trades);
            }
        }).start();
    }

    protected static void uploadPair(final JSONObject pair, final JLabel pairedUploadedLbl, final JProgressBar pairedUploadedProgress) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (helper.uploadPair(pair)) {
                    pairedUploadedProgress.setValue(pairedUploadedProgress.getValue() + 1);
                    setPairUploadedText(pairedUploadedLbl, pairedUploadedProgress.getValue());
                }
            }
        }).start();
    }

    protected static Document getPageDocument(String url) {
        String html = helper.get(url);
        return Jsoup.parse(html);
    }

    protected static Document getPageDocumentProxy(String url) {
        String html = helper.getViaProxy(url);
        return Jsoup.parse(html);
    }

}
