package kg.qsoft.ds.parser.walmart;

import kg.qsoft.ds.parser.AbstractParser;
import kg.qsoft.ds.parser.amazon.AmazonSimilarTradeParser;
import kg.qsoft.ds.parser.ebay.EbaySimilarTradeParser;
import kg.qsoft.ds.parser.page.WalmartTradePageParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.nodes.Document;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.io.InterruptedIOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by akoikelov
 */
public class WalmartParser extends AbstractParser {

    public static boolean parse(String currentUrl, int page, JProgressBar progress, JTable resultTable, JLabel uploadedLbl, int quantity, final JLabel pairedUploadedLbl, final JProgressBar pairedUploadedProgress, JCheckBox onlyPairedTrades, int maxPageNumber) throws InterruptedIOException {
        final DefaultTableModel model = (DefaultTableModel) resultTable.getModel();
        final JSONArray trades = new JSONArray();
        final boolean onlyPaired = onlyPairedTrades.isSelected();

        if (!currentUrl.contains("?")) {
            currentUrl += "?";
        }

        String url = currentUrl.replace("#searchProductResult", "") + String.format("&page=%s", page);
        url = url.contains("grid=true") ? url : url + "&grid=true";

        Document doc = getPageDocument(url);

        String itemsRegex = "\"itemIds\":\"([0-9\\,]+)\"";
        Pattern itemsPattern = Pattern.compile(itemsRegex);
        Matcher itemsMatcher = itemsPattern.matcher(doc.html());

        if (maxPageNumber == -1) {
            String paginationRegex = "\\{\"page\":([0-9]+)([^}]+)\"selected\":false\\}\\]";
            Pattern paginationPattern = Pattern.compile(paginationRegex);
            Matcher paginationMatcher = paginationPattern.matcher(doc.html());

            if (paginationMatcher.find()) {
                maxPageNumber = Integer.valueOf(paginationMatcher.group(1));
            }
        }

        if (itemsMatcher.find()) {
            for (final String id : itemsMatcher.group(1).split(",")) {
                final JSONObject trade = WalmartTradePageParser.parse(String.format(WALMART_TRADE_URL, id));

                if (trade == null) {
                    continue;
                }

                trade.put("trade_id", id);
                trades.add(trade);

                if (!onlyPaired) {
                    addRowToModelTable(model, trade);
                }

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (!trade.get("upc").equals("-")) {
                            JSONObject amazonTrade = AmazonSimilarTradeParser.parseSimilarTrade((String) trade.get("upc"));
                            JSONObject ebayTrade = EbaySimilarTradeParser.parseSimilarTrade((String) trade.get("upc"));

                            if (amazonTrade != null) {
                                JSONObject pairTrade = new JSONObject();
                                pairTrade.put("amazon", amazonTrade);
                                pairTrade.put("ebay", ebayTrade);
                                pairTrade.put("walmart", trade);

                                uploadPair(pairTrade, pairedUploadedLbl, pairedUploadedProgress);

                                if (onlyPaired) {
                                    addRowToModelTable(model, trade);
                                }
                            }
                        }
                    }
                }).start();
            }

            if (!onlyPaired) {
                quantity += trades.size();
                setUploadedText(uploadedLbl, quantity);

                uploadTrades(trades);
            }
        }

        if (page < maxPageNumber) {
            progress.setValue(page);
            page++;
            return parse(currentUrl, page, progress, resultTable, uploadedLbl, quantity, pairedUploadedLbl, pairedUploadedProgress, onlyPairedTrades, maxPageNumber);
        } else {
            progress.setValue(500);
            return true;
        }
    }

    private static void addRowToModelTable(DefaultTableModel model, JSONObject trade) {
        model.addRow(new Object[] {
            trade.get("trade_id"), trade.get("title"), trade.get("price"),
            trade.get("brand"), trade.get("rating"), trade.get("upc"), trade.get("shipping")
        });
    }

}
