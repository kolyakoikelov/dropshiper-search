package kg.qsoft.ds.parser.walmart;

import com.sun.istack.internal.Nullable;
import kg.qsoft.ds.parser.AbstractSimilarTradeParser;
import kg.qsoft.ds.parser.page.WalmartTradePageParser;
import org.json.simple.JSONObject;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by akoikelov
 */
public class WalmartSimilarTradeParser extends AbstractSimilarTradeParser {

    public static JSONObject parseSimilarTrade(String upc) {
        String currentUrl = String.format(WALMART_SEARCH_URL, upc);

        Document doc = getPageDocument(currentUrl);
        Elements tradesList = doc.select("ul.search-result-gridview-items > li");

        if (tradesList.size() > 0) {
            Element item = tradesList.get(0);
            Elements link = item.getElementsByTag("a");

            if (link.size() > 0) {
                String linkValue = link.get(0).attr("href");
                String[] linkParts = linkValue.split("/");

                Elements picture = item.getElementsByClass("Tile-img");
                JSONObject trade = WalmartTradePageParser.parse(WALMART_URL + linkValue);

                if (trade != null) {
                    trade.put("trade_id", linkParts[linkParts.length - 1]);

                    if (picture.size() > 0) {
                        trade.put("picture", picture.get(0).attr("src"));
                    } else {
                        trade.put("picture", "-");
                    }

                    return trade;
                }
            }
        }

        return null;
    }

}
