package kg.qsoft.ds.parser.ebay;

import kg.qsoft.ds.parser.AbstractSimilarTradeParser;
import kg.qsoft.ds.parser.page.EbayTradePageParser;
import org.json.simple.JSONObject;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by akoikelov
 */
public class EbaySimilarTradeParser extends AbstractSimilarTradeParser {

    public static JSONObject parseSimilarTrade(String upc) {
        String currentUrl = String.format(EBAY_SEARCH_URL, upc);

        Document doc = getPageDocumentProxy(currentUrl + String.format("&_pgn=%s", 1));
        Elements tradesList = doc.getElementsByClass("sresult");

        if (tradesList.size() > 0) {
            Element item = tradesList.get(0);
            Elements links = item.getElementsByTag("a");
            Elements fastnFree = item.getElementsByClass("FnFl");

            if (links.size() > 0) {
                JSONObject trade = EbayTradePageParser.parse(String.format(EBAY_TRADE_URL, item.attr("listingid")));
                trade.put("trade_id", item.attr("listingid"));

                if (fastnFree.size() > 0) {
                    trade.put("fastnfree", true);
                } else {
                    trade.put("fastnfree", false);
                }

                return trade;

            }
        }

        return null;
    }

}
