package kg.qsoft.ds.parser.ebay;

import kg.qsoft.ds.parser.AbstractParser;
import kg.qsoft.ds.parser.amazon.AmazonSimilarTradeParser;
import kg.qsoft.ds.parser.page.EbayTradePageParser;
import kg.qsoft.ds.parser.walmart.WalmartSimilarTradeParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.io.InterruptedIOException;

/**
 * Created by akoikelov
 */
public class EbayParser extends AbstractParser {

    public static boolean parse(String url, int page, JProgressBar progress, JTable resultTable, JLabel uploadedLbl, int quantity, final JLabel pairedUploadedLbl, final JProgressBar pairedUploadedProgress, JCheckBox filterSellingTrades, JCheckBox onlyPairedTrades) throws InterruptedIOException {
        Document doc = getPageDocumentProxy(url + String.format("&_pgn=%s", page));
        final DefaultTableModel model = (DefaultTableModel) resultTable.getModel();
        final JSONArray trades = new JSONArray();
        Elements tradesList = doc.getElementsByClass("sresult");
        final boolean onlyPaired = onlyPairedTrades.isSelected();

        for (Element item : tradesList) {
            Elements links = item.getElementsByTag("a");
            Elements fastnFree = item.getElementsByClass("FnFl");

            if (links.size() > 0) {
                final JSONObject trade = EbayTradePageParser.parse(String.format(EBAY_TRADE_URL, item.attr("listingid")));

                if (trade == null) {
                    continue;
                }

                if (filterSellingTrades.isSelected() && Integer.valueOf((String) trade.get("sold_score")) == 0) {
                    continue;
                }

                trade.put("trade_id", item.attr("listingid"));

                if (fastnFree.size() > 0) {
                    trade.put("fastnfree", true);
                } else {
                    trade.put("fastnfree", false);
                }

                if (!onlyPaired) {
                    addRowToModelTable(model, trade);
                }

                trades.add(trade);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (!trade.get("upc").equals("-")) {
                            JSONObject amazonTrade = AmazonSimilarTradeParser.parseSimilarTrade((String) trade.get("upc"));
                            JSONObject walmartTrade = WalmartSimilarTradeParser.parseSimilarTrade((String) trade.get("upc"));

                            if (amazonTrade != null) {
                                JSONObject pairTrade = new JSONObject();
                                pairTrade.put("amazon", amazonTrade);
                                pairTrade.put("ebay", trade);
                                pairTrade.put("walmart", walmartTrade);

                                uploadPair(pairTrade, pairedUploadedLbl, pairedUploadedProgress);

                                if (onlyPaired) {
                                    addRowToModelTable(model, trade);
                                }
                            }
                        }
                    }
                }).start();
            }
        }

        if (!onlyPaired) {
            quantity += trades.size();
            setUploadedText(uploadedLbl, quantity);

            uploadTrades(trades);
        }

        Elements next = doc.getElementsByClass("next");
        if (next.size() > 0) {
            page++;
            return parse(url, page, progress, resultTable, uploadedLbl, quantity, pairedUploadedLbl, pairedUploadedProgress, filterSellingTrades, onlyPairedTrades);
        } else {
            progress.setValue(page);
            return true;
        }
    }

    private static void addRowToModelTable(DefaultTableModel model, JSONObject trade) {
        model.addRow(new Object[] {
             trade.get("trade_id"), trade.get("title"), trade.get("price"),
             trade.get("shipping"), trade.get("feedback_score"), trade.get("positive_feedback"),
             (Boolean) trade.get("fastnfree") ? "yes" : "no", trade.get("upc"), trade.get("sold_score")
        });
    }

}
